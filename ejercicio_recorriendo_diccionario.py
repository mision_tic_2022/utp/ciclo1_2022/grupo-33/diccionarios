

curso = {
    '1234': {
        'nombre': 'Andrés',
        'apellido': 'Quintero',
        'notas': {
            'nota_1': 4.8,
            'nota_2': 3.1,
            'nota_3': 4.9,
            'nota_4': 4.5
        }
    },
    '4321': {
        'nombre': 'Ana María',
        'apellido': 'Muñoz',
        'notas': {
            'nota_1': 4.8,
            'nota_2': 3.8,
            'nota_3': 4.1,
            'nota_4': 4.2
        }
    },
    '7890': {
        'nombre': 'Juan',
        'apellido': 'Alvarez',
        'notas': {
            'nota_1': 5,
            'nota_2': 2.8,
            'nota_3': 3.1,
            'nota_4': 4.9
        }
    },
    '5678': {
        'nombre': 'Sandy',
        'apellido': 'Medina',
        'notas': {
            'nota_1': 4.6,
            'nota_2': 4.8,
            'nota_3': 3.7,
            'nota_4': 4.2
        }
    }
}


def calcular_promedio_notas(dict_curso: dict):
    #Variable que representa un diccionario con el promeido de notas de los estudiantes
    dict_promedio_notas = dict()
    #Itera diccionario 'curso'
    for cedula,estudiante in dict_curso.items():
        #Obtener las notas del estudiante
        notas = estudiante['notas']
        #Variable que representa el promedio de notas de cada estudiante
        promedio_estudiante = 0
        #Recorrer el diccionario 'notas'
        #For/ciclo anidado
        for n in notas.values():
            #Sumar las notas del estudiante
            #promedio_estudiante = promedio_estudiante + n
            promedio_estudiante += n
            #print(n)
        #Obtener el tamaño del diccionario 'notas'
        cant_notas = len(notas)
        #Calcular el promedio de las notas del estudiante
        promedio_estudiante /= cant_notas
        #round -> redondear un valor a una cantidad de decimales en específico
        #round(numero_a_redondear, cantidad_decimales)
        #Añadir promedio al diccionario 'dict_promedio_notas'
        dict_promedio_notas[cedula] = round(promedio_estudiante, 1)
    #Retorno el diccionario que contiene el promedio de las notas
    return dict_promedio_notas


def calcular_promedio_general(promedio_estudiantes: dict):
    promedio_general = 0
    for n in promedio_estudiantes.values():
        promedio_general += n
    return round(promedio_general / len(promedio_estudiantes))


promedio_notas = calcular_promedio_notas(curso)
print(promedio_notas)
print('-------')

print( calcular_promedio_general(promedio_notas) )