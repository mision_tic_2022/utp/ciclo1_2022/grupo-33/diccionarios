
#Diccionario vacío
miDiccionario = {}
miDiccionario = dict()

print(miDiccionario)

#Diccionario con la info de un estudiante
estudiante_1 = {
    'nombre': 'Isaack',
    'apellido': 'Vallejo',
    'edad': 25,
    'promedio_notas': 4.2,
    'activo': True
}

x = estudiante_1['apellido']
print(x)

edad = estudiante_1['edad']
print(edad)


print('--------------------------------------')

estudiante_1 = {
    'nombre': 'Isaack',
    'apellido': 'Vallejo',
    'edad': 25,
    'promedio_notas': 4.2,
    'activo': True
}

estudiante_2 = {
    'nombre': 'Milfer',
    'apellido': 'Padilla',
    'edad': 28,
    'promedio_notas': 0,
    'activo': False
}

'''
Un diccionario está compuesto de una clave y un valor
'''
estudiantes = {
    '12345': estudiante_1,#Clave-valor es un item
    '54321': estudiante_2
}

print(estudiantes)

print('----------------------AÑADIENDO ELEMENTOS AL DICCIONARIO-----------------------')

registro = {}
print(registro)

registro['nombre'] = 'Andrés'
registro['apellido'] = 'Gomez'
registro['email'] = 'andres@gmail.com'
registro['password'] = '123456'
registro['celular'] = '31234567890'

print(registro)


'''
miDiccionario = {
    'clave_1': {'mensaje': 'Hola'},
    'clave_2': 12,
    'clave_3': 10
}

print( len(miDiccionario) )

num_1 = 10
num_2 = 2

#num_1 = num_1 / num_2
num_1 /= num_2

print(num_1)
'''