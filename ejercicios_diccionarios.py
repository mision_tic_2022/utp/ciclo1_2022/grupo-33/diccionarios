
estudiantes = {
    '1234': {
        'nombre': 'Andrés',
        'edad': 20
    },
    '9876': {
        'nombre': 'Alejandra',
        'edad': 24
    },
    '1155': {
        'nombre': 'Sebastián',
        'edad': 25
    }
}
'''
1) Calcular el promedio de edad de los estudiantes a partir del diccionario notas_estudiantes
'''
estudiante_1 = estudiantes['1234']
edad_1 = estudiante_1['edad']

estudiante_2 = estudiantes['9876']
edad_2 = estudiante_2['edad']

estudiante_3 = estudiantes['1155']
edad_3 = estudiante_3['edad']

promedio_edad = (edad_1 + edad_2 + edad_3) / 3
print(promedio_edad)


'''
1) Calcule el promedio de ventas de cada sede del supermercado
2) Calcule el TOTAL de ventas del supermercado
'''

supermercado = {
    'belen':{
        'aseo': 12000000,
        'frutas': 25000000
    },
    'estadio':{
        'aseo': 8000000,
        'frutas': 30000000
    }
}

#Solución de Luis Miguel
belen = supermercado['belen']
estadio = supermercado['estadio']

promBelen = (belen['aseo']+belen['frutas'])/2
print(promBelen)
promEstadio = (estadio['aseo']+estadio['frutas'])/2
print(promEstadio)

total = belen['aseo']+belen['frutas']+estadio['aseo']+estadio['frutas']
print(total)


'''
Para la clase 11 de Mayo temas que se DEBERÁN ver:
    *Recorrido de diccionarios
    *ciclos (while)
    *listas
    *tuplas
    *conjuntos
'''