
'''
ventas = {
    'ventas_1': 10,
    'ventas_2': 15,
    'ventas_3': 11
}

#variable que almacena la sumatoria de todas las ventas
suma = 0
#Iterar el diccionario y obtener las claves
for x in ventas:
    #Obtener el valor de cada item
    y = ventas[x]
    #Sumar
    suma = suma + y

print(suma)
'''


ventas = {
    'ventas_1': 10,
    'ventas_2': 15,
    'ventas_3': 11
}

print('----------------CLAVES---------------')
#Iteración obteniendo las claves del diccinario
for x in ventas:
    print(x)

print('----------------VALORES-----------------')
#Iteración obteniendo los valores del diccionario
for x in ventas.values():
    print(x) 

print('----------------CLAVE - VALOR-----------------')
#Iteración obteniendo clave-valor
for x,y in ventas.items():
    print('clave: ', x)
    print('valor: ', y)


print('-------------------------------------------------------------------')

supermercado = {
    'ciudad': 'Medellín',
    'belen':{
        'aseo': 12000000,
        'frutas': 25000000
    },
    'estadio':{
        'aseo': 8000000,
        'frutas': 30000000
    }
}

for key in supermercado:
    print(key) 

print("----VALUE-----")

for value in supermercado.values():
    print(value)

print("----------")

for key,value in supermercado.items():
    print(key)
    print(value)